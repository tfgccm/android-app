package es.upm.sidocs.sidocs.data.remote;

import android.content.Context;

public class APIUtils {

    private APIUtils() {}

    public static final String BASE_URL = "https://ccmanzanero.pythonanywhere.com/";
    //public static final String BASE_URL = "http://192.168.0.160:8000/";

    public static APIInterface getAPIService(Context context, int tipo) {

        // Se define como tipo 1 el cliente encargado de resfrescar el token de acceso dentro del cliente APIClient
        if(tipo == 1){
            return APIClientRefrescar.getClientRefrescar(BASE_URL).create(APIInterface.class);
        }
        // Se define como tipo 0 el cliente encargado de realizar el resto de operaciones REST
        else
            return APIClient.getClient(BASE_URL, context).create(APIInterface.class);
    }
}