package es.upm.sidocs.sidocs.data.remote;
import es.upm.sidocs.sidocs.data.model.Humedad;
import es.upm.sidocs.sidocs.data.model.IPPublica;
import es.upm.sidocs.sidocs.data.model.ListadoAlarmas;
import es.upm.sidocs.sidocs.data.model.ModoFuncionamientoActualizar;
import es.upm.sidocs.sidocs.data.model.ModoFuncionamientoLeer;
import es.upm.sidocs.sidocs.data.model.Temperatura;
import es.upm.sidocs.sidocs.data.model.UsuarioAcceso;
import es.upm.sidocs.sidocs.data.model.UsuarioAccesoRespuesta;
import es.upm.sidocs.sidocs.data.model.UsuarioRegistro;
import es.upm.sidocs.sidocs.data.model.UsuarioRegistroFCM;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Header;
import retrofit2.http.Body;
import retrofit2.http.Path;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import rx.Observable;
import java.util.List;

public interface APIInterface {

    // Actividad Registro
    @POST("/api/registrar/")
    Observable<Response<UsuarioRegistro>> registrar(
                @Body UsuarioRegistro body);

    // Actividad Acceso
    @POST("/api/acceder/")
    Observable<Response<UsuarioAccesoRespuesta>> acceder(
            @Body UsuarioAcceso body);

    // (Petición síncrona para obtención de tokenJWT tras expiración)
    @POST("/api/acceder/")
    Call<UsuarioAccesoRespuesta> reAcceder(
            @Body UsuarioAcceso body);

    @POST("/api/dispositivos/registrar/")
    Observable<Response<UsuarioRegistroFCM>> registrarFCM(
            @Body UsuarioRegistroFCM body, @Header("Authorization") String token);

    // Actividad MenuPrincipal
    @DELETE("/api/dispositivos/eliminar/{registration_id}")
    Observable<Response<UsuarioRegistroFCM>> eliminarFCM(
            @Path("registration_id") String registration_id, @Header("Authorization") String token);

    @GET("/api/modolast/")
    Observable<Response<List<ModoFuncionamientoLeer>>> leerModoFuncionamiento(
            @Header("Authorization") String token);

    // Actividad Sensores
    @GET("/api/temperaturalast/")
    Observable<Response<List<Temperatura>>> leerTemperatura(
            @Header("Authorization") String token);

    @GET("/api/humedadlast/")
    Observable<Response<List<Humedad>>> leerHumedad(
            @Header("Authorization") String token);

    // Actividad Configuracion
    @PUT("/api/modo/{id}")
    Observable<Response<ModoFuncionamientoLeer>> actualizarModoFuncionamiento(
            @Path("id") Integer id, @Body ModoFuncionamientoActualizar body, @Header("Authorization") String token);

    // Actividad Camara
    @GET("/api/iplast/")
    Observable<Response<List<IPPublica>>> leerIPPublica(
            @Header("Authorization") String token);

    // Actividad Alarmas
    @GET("/api/alarmalast/")
    Observable<Response<List<ListadoAlarmas>>> leerAlarmas(
            @Header("Authorization") String token);

}