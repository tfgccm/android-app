package es.upm.sidocs.sidocs.data.remote;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

class APIClientRefrescar {

    private static Retrofit retrofitRefrescar = null;

    static Retrofit getClientRefrescar(String baseUrl) {

        if (retrofitRefrescar == null) {

            retrofitRefrescar = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofitRefrescar;
    }
}
