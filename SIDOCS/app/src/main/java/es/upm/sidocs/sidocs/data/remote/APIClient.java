package es.upm.sidocs.sidocs.data.remote;
import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;
import java.io.IOException;
import es.upm.sidocs.sidocs.data.model.UsuarioAcceso;
import es.upm.sidocs.sidocs.data.model.UsuarioAccesoRespuesta;
import es.upm.sidocs.sidocs.utils.UserSharedPreferences;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Authenticator;
import okhttp3.Route;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

class APIClient {

    // Tag identificativo para Logcat
    public static final String TAG = "SIDOCS_APIClient";

    private static Retrofit retrofit = null;

    // Instancia de APIInterface para establecer las peticiones de refresco de token
    private static APIInterface apiClientRefrescar;


    static Retrofit getClient(String baseUrl, Context context) {

        // Se crea un cliente OkHttp para posteriormente instanciar un authenticator
        final OkHttpClient.Builder client = new OkHttpClient.Builder();

        // Se inicializa el cliente Retrofit2 con el tipo '1' y el contexto obtenido de la actividad
        apiClientRefrescar= APIUtils.getAPIService(context, 1);

        if (retrofit==null) {

            // Se redefine el método authenticate que permitirá capturar las respuestas 401 del servidor y
            // obtener un nuevo token JWT

            client.authenticator(new Authenticator() {

                @Override
                public Request authenticate(Route route, Response response) throws IOException {

                    String tokenJWT_actual = response.request().header("Authorization");
                    String tokenJWT_guardado = UserSharedPreferences.getTokenJWT(context);

                    Log.i(TAG, "El token empleado en la petición es: " + tokenJWT_actual);
                    Log.i(TAG, "El token almacenado en SharedPreferences es: " + tokenJWT_guardado);

                    if (!tokenJWT_actual.equals(tokenJWT_guardado)) {
                        Log.i(TAG, "El token JWT ya ha sido refrescado");
                        return null;
                    }

                    // Se inicializa la variable tokenJWT_nuevo a null
                    String tokenJWT_nuevo = null;

                    // Se obtienen las credenciales del usuario guardadas en SharedPreferences
                    String usuario = UserSharedPreferences.getUsuario(context);
                    String password = UserSharedPreferences.getPassword(context);
                    // Se crea una instacia de UsuarioAcceso con las credenciales obtenidas de SharedPreferences
                    UsuarioAcceso usuarioAcceso = new UsuarioAcceso(usuario,password);

                    // Se ejecuta una petición síncrona de acceso al servidor REST para obtener un nuevo token JWT
                    Call<UsuarioAccesoRespuesta> call = apiClientRefrescar.reAcceder(usuarioAcceso);
                    try {

                        UsuarioAccesoRespuesta responseUsuarioAcceso = call.execute().body();
                        if (responseUsuarioAcceso != null) {

                            // Se obtiene el nuevo token de la respuesta a la petición de acceso
                            tokenJWT_nuevo = responseUsuarioAcceso.getToken();
                            // Se guarda el nuevo token en SharedPreferences
                            UserSharedPreferences.setTokenJWT(context,tokenJWT_nuevo);
                        }
                    }
                    catch (Exception oe) {
                        oe.printStackTrace();
                    }

                    // Si la petición de nuevo token ha sido satisfactoria y por tanto la variable tokenJWT_nuevo es distinto a null
                    if (tokenJWT_nuevo != null) {
                        Log.i(TAG, "Reintentando petición tras el refresco de token");

                        // Reejecutamos las peticiones que han fallado
                        return response.request().newBuilder()
                                .header("Authorization", UserSharedPreferences.getTokenJWT(context))
                                .build();
                    }
                    else
                        return null;
                }
            });

            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client.build())
                    .build();
        }
        return retrofit;
    }
}
