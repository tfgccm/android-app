package es.upm.sidocs.sidocs;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import es.upm.sidocs.sidocs.data.model.UsuarioRegistro;
import es.upm.sidocs.sidocs.data.remote.APIInterface;
import es.upm.sidocs.sidocs.data.remote.APIUtils;
import es.upm.sidocs.sidocs.utils.UserProgressDialog;
import retrofit2.Response;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class Registro extends AppCompatActivity {

    // Tag identificativo para Logcat
    public static final String TAG = "SIDOCS_Registro";

    // Ventanta de carga para el proceso de registro
    private UserProgressDialog pd;

    // Instancia de APIInterface para establecer las peticiones al servidor REST
    private APIInterface servicioAPI;

    // Vistas para obtener los datos del usuario y botón para ejecutar el registro
    private EditText it_Usuario, it_Email, it_Password, it_Password_rep;
    private TextInputLayout it_Layout_Usuario, it_Layout_Email, it_Layout_Password, it_Layout_Password_rep;
    private Button b_registrar;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        // Se crea y se muestra una ventana de carga
        // Se crea una ventana de carga
        pd = new UserProgressDialog(this,
                getString(R.string.titulo_registro),
                getString(R.string.mensaje_registro));

        // Creación de las vistas en la creación de la actividad
        it_Usuario = (EditText) findViewById(R.id.id_it_registrar_usuario);
        it_Email = (EditText) findViewById(R.id.id_it_registrar_email);
        it_Password = (EditText) findViewById(R.id.id_it_registrar_password);
        it_Password_rep = (EditText) findViewById(R.id.id_it_registrar_rep_password);
        it_Layout_Usuario = (TextInputLayout) findViewById(R.id.id_it_registrar_usuario_layout);
        it_Layout_Email = (TextInputLayout) findViewById(R.id.id_it_registrar_email_layout);
        it_Layout_Password = (TextInputLayout) findViewById(R.id.id_it_registrar_password_layout);
        it_Layout_Password_rep = (TextInputLayout) findViewById(R.id.id_it_registrar_rep_password_layout);
        b_registrar = (Button) findViewById(R.id.id_b_registrar);

        b_registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registrar(view);
            }
        });

        servicioAPI = APIUtils.getAPIService(getApplicationContext(),0);

    }

    public void ejecutarAcceso(View view){

        Intent i = new Intent(this, Acceso.class);
        startActivity(i);

    }

    private void registrar(View view) {

        // Variables auxiliares de validación
        boolean val_usr, val_eml, val_pwd, val_pwdr;

        // Se ejecuta la validación de todos los campos del formulario y
        // se recupera el resultado de cada validación en una variable auxiliar
        val_usr = validarUsuario();
        val_eml = validarEmail();
        val_pwd = validarPassword();
        val_pwdr = validarPasswordRep();
        Log.e(TAG, "Validando valores del formulario...");

        // Se comprueba si la validación ha sido satisfactoria
        if (val_usr && val_eml && val_pwd && val_pwdr ) {

            // Se muestra la ventana de carga
            pd.mostrar();

            Log.i(TAG, "Datos Validados");

            // Se quitan posibles espacios en blanco a izquierda y derecha de la cadena de texto
            String usuario = it_Usuario.getText().toString().trim();
            String email = it_Email.getText().toString().trim();

            String password = it_Password.getText().toString();
            String password2 = it_Password_rep.getText().toString();

            // Se crea una instacia de UsuarioRegistro con los datos introducidos por el usuarios
            UsuarioRegistro body = new UsuarioRegistro(usuario, email, password, password2);

            // Éxito en la validación, por tanto se ejecuta la llamada al servidor REST
            registrarAPI(body, view);

        }
    }

    private boolean validarUsuario() {
        //Si el campo está vacío (o sólo se han introducido espacios en blanco) se muestra el error
        if (it_Usuario.getText().toString().trim().isEmpty()) {
            it_Layout_Usuario.setErrorEnabled(true);
            it_Layout_Usuario.setError(getString(R.string.error_campo_vacio));
            return false;
        } else {
            // Al no haber errores, se deshabilita la funcionalidad de error
            it_Layout_Usuario.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validarEmail() {

        String email = it_Email.getText().toString().trim();

        if (email.isEmpty()) {
            it_Layout_Email.setErrorEnabled(true);
            it_Layout_Email.setError(getString(R.string.error_campo_vacio));
            return false;

        } else if (!emailFormato(email)) {
            it_Layout_Email.setErrorEnabled(true);
            it_Layout_Email.setError(getString(R.string.error_email_formato));
            return false;
        }

        else {
            it_Layout_Email.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validarPassword() {

        if (it_Password.getText().toString().trim().isEmpty()) {
            it_Layout_Password.setErrorEnabled(true);
            it_Layout_Password.setError(getString(R.string.error_campo_vacio));
            return false;
        } else {
            it_Layout_Password.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validarPasswordRep() {

        if (it_Password_rep.getText().toString().trim().isEmpty()) {
            it_Layout_Password_rep.setErrorEnabled(true);
            it_Layout_Password_rep.setError(getString(R.string.error_campo_vacio));
            return false;
        } else {
            it_Layout_Password_rep.setErrorEnabled(false);
        }

        return true;
    }


    // Validación del formato del email introducido por el usuario
    private static boolean emailFormato(String email) {

        if(!TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches())
            return true;
        else
            return false;
    }

    public void registrarAPI(UsuarioRegistro params, final View view) {

        servicioAPI.registrar(params)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Subscriber<Response<UsuarioRegistro>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                        // Se elimina la ventana de carga
                        pd.ocultar();
                        Log.e(TAG, "ERROR en la llamada a la API.");
                        Toast.makeText(getApplicationContext(), getString(R.string.error_registro_usuario), Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onNext(Response<UsuarioRegistro> response) {
                        // Se elimina la ventana de carga
                        pd.ocultar();

                        // Variable para la obtención del resultado de cada petición
                        int codigoPeticion = response.code();
                        // Variables auxiliares para la obtención de la respuesta del servidor y eliminación de caracteres sobrantes de la misma
                        String auxUser, clean_auxUser, auxEmail, clean_auxEmail, auxPassword, clean_auxPassword, auxPassword2, clean_auxPassword2;

                        // La petición ha sido ejecutada correctamente y el usuario se ha registrado en el sistema
                        if (codigoPeticion == 201){

                            Toast.makeText(getApplicationContext(), getString(R.string.registro_correcto), Toast.LENGTH_SHORT).show();

                            // Se limpia el formulario de acceso
                            it_Usuario.getText().clear();
                            it_Email.getText().clear();
                            it_Password.getText().clear();
                            it_Password_rep.getText().clear();

                            // Si el registro es correcto, el usuario accede a la actividad "Acceso"
                            ejecutarAcceso(view);
                        }

                        // La petición ha sido ejecutada correctamente pero ha habido un error de validación de datos en el servidor
                        else if(codigoPeticion==400) {

                            try {
                                String response_error = response.errorBody().string();
                                try{

                                    // Se crea un objeto JSON para almacenar el resultado de la petición 400, ya que una vez se ha accedido
                                    // al contenido de response.errorBody().string() se referencia a null y el resultado de la petición se perdería
                                    JSONObject jsonError = new JSONObject (response_error);

                                    // Se comprueba si hay un elemeto "nombre" en el objeto JSON
                                    if(!jsonError.isNull("username")) {
                                        // Objeto JSON auxiliar para almacenar el resultado de la petición con resultado 400
                                        auxUser = jsonError.getString("username");
                                        // Se eliminan los caracteres [" y "] de la cadena recibida
                                        clean_auxUser = auxUser.replace("[\"", "").replace("\"]", "");
                                        // Se muestra el mensaje resultante del servidor (si lo hubiese) en la interfaz del campo
                                        it_Layout_Usuario.setError(clean_auxUser);
                                    }

                                    // Se repite el proceso con el resto de campos
                                    if(!jsonError.isNull("email")) {
                                        auxEmail = jsonError.getString("email");
                                        clean_auxEmail = auxEmail.replace("[\"", "").replace("\"]", "");
                                        it_Layout_Email.setError(clean_auxEmail);
                                    }
                                    if(!jsonError.isNull("password")) {
                                        auxPassword = jsonError.getString("password");
                                        clean_auxPassword = auxPassword.replace("[\"", "").replace("\"]", "");
                                        it_Layout_Password.setError(clean_auxPassword);
                                    }
                                    if(!jsonError.isNull("password2")) {
                                        auxPassword2 = jsonError.getString("password2");
                                        clean_auxPassword2 = auxPassword2.replace("[\"", "").replace("\"]", "");
                                        it_Layout_Password_rep.setError(clean_auxPassword2);
                                    }
                                } catch(JSONException je){
                                    je.printStackTrace();

                                }
                            } catch (IOException ie) {
                                ie.printStackTrace();
                            }
                        }
                    }
                });
    }

}
