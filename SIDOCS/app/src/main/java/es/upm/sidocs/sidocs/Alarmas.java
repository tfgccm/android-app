package es.upm.sidocs.sidocs;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import es.upm.sidocs.sidocs.data.model.ListadoAlarmas;
import es.upm.sidocs.sidocs.data.remote.APIInterface;
import es.upm.sidocs.sidocs.data.remote.APIUtils;
import es.upm.sidocs.sidocs.utils.UserProgressDialog;
import es.upm.sidocs.sidocs.utils.UserSharedPreferences;
import retrofit2.Response;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import android.support.v7.widget.LinearLayoutManager;

public class Alarmas extends AppCompatActivity {

    // Tag identificativo para Logcat
    public final String TAG = "SIDOCS_Alarmas";

    // Ventanta de carga para el proceso de acceso
    private UserProgressDialog pd;

    // Instancia de APIInterface para establecer las peticiones al servidor REST
    private APIInterface servicioApi;

    // Vistas para obtener las credenciales del usuario y botón para ejecutar el acceso
    List<ListadoAlarmas> listadoAlarmas;
    AlarmasAdapter alarmasAdapter;
    RecyclerView recyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarmas);


        // Se crea una ventana de carga
        pd = new UserProgressDialog(this,
                getString(R.string.titulo_cargar_alarmas),
                getString(R.string.cuerpo_cargar_alarmas));

        servicioApi = APIUtils.getAPIService(getApplicationContext(),0);

        listadoAlarmas = new ArrayList<>();
        recyclerView =  (RecyclerView) findViewById(R.id.id_recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        // Se muestra la ventana de carga
        pd.mostrar();
        // Se obtiene el valor del token JWT de Shared Preferences
        String tokenJWT = UserSharedPreferences.getTokenJWT(Alarmas.this);
        // Se ejecuta la llamada a la API REST para obtener los valores al inicio de la Actividad
        leerAlarmasAPI(tokenJWT);
    }


    public void leerAlarmasAPI(String tokenJWT) {

        servicioApi.leerAlarmas(tokenJWT)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<List<ListadoAlarmas>>>() {

                    @Override
                    public void onCompleted() {

                        // Petición finalizada
                        pd.ocultar();
                    }

                    @Override
                    public void onError(Throwable e) {

                        // Petición finalizada
                        pd.ocultar();
                        Log.e(TAG, "Error de conexión con el servidor en la lectura del listado de Alarmas");

                    }

                    @Override
                    public void onNext(Response<List<ListadoAlarmas>> response) {

                        int codigoPeticion = response.code();

                        // La petición ha sido ejecutada correctamente

                        if (codigoPeticion == 200) {

                            try {
                                listadoAlarmas = response.body();
                                alarmasAdapter = new AlarmasAdapter(getApplicationContext(),listadoAlarmas);
                                recyclerView.setAdapter(alarmasAdapter);


                            } catch (Exception oe) {
                                oe.printStackTrace();
                            }

                            // La petición ha sido ejecutada correctamente pero el token JWT usado ha expirado o no se ha referenciado en la cabecera
                        } else if (codigoPeticion == 401) {

                            Toast.makeText(getApplicationContext(), getString(R.string.error_lectura_alarmas), Toast.LENGTH_SHORT).show();

                        }
                    }
                });
    }
                
}
