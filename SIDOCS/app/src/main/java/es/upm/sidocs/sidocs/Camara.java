package es.upm.sidocs.sidocs;
import android.net.http.SslError;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import java.util.List;
import es.upm.sidocs.sidocs.data.model.IPPublica;
import es.upm.sidocs.sidocs.data.remote.APIInterface;
import es.upm.sidocs.sidocs.data.remote.APIUtils;
import es.upm.sidocs.sidocs.utils.UserProgressDialog;
import es.upm.sidocs.sidocs.utils.UserSharedPreferences;
import retrofit2.Response;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class Camara extends AppCompatActivity {

    // Tag identificativo para Logcat
    public final String TAG = "SIDOCS_Camara";

    // Ventana de carga para la lectura de IPPublica de RBPi
    private UserProgressDialog pd;

    // Instancia de APIInterface para establecer las peticiones al servidor REST
    private APIInterface servicioApi;

    // Variables auxiliares necesarias para almacenar la IPPublica de RBPi y componer la URL
    public String ippublica, url;

    // Vistas para acceder a la reproducción en directo de la cámara de seguridad
    private WebView navegador;
    private View b_refrescar;


    // Variables auxiliares para la configuración de la url UV4L
    private static String usuario = "user";
    private static String password = "carlos21";
    private static String puerto = "9000";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camara);

        // Creamos una ventana de carga de lectura de IPPublica
        pd = new UserProgressDialog(this,
                getString(R.string.titulo_cargar_camara),
                getString(R.string.cuerpo_cargar_camara));

        servicioApi = APIUtils.getAPIService(getApplicationContext(),0);

        // Asignamos las instancias creadas con las vistas
        b_refrescar = (View) findViewById(R.id.id_b_refrescar);
        navegador = (WebView) findViewById(R.id.id_navegador_camara);

        // Ejecutamos el cliente WebView sobreescribiendo el médoto "onReceivedSslError"
        navegador.setWebViewClient(new WebViewClient(){
            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                // De esta forma ignoramos el error generado por el autocertificado
                handler.proceed();
            }
        });

        // Ejecutamos la llamada a la API REST para obtener el valor de la IPPublica y cargar la URL
        leerIPPublica();

        b_refrescar.setOnClickListener(view -> leerIPPublica());

    }
    private void leerIPPublica(){

        pd.mostrar();
        String tokenJWT = UserSharedPreferences.getTokenJWT(Camara.this);
        leerIPPublicaAPI(tokenJWT);
    }

    public void leerIPPublicaAPI(String tokenJWT){

        servicioApi.leerIPPublica(tokenJWT)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<List<IPPublica>>>() {

                    @Override
                    public void onCompleted() {
                        pd.ocultar();
                    }

                    @Override
                    public void onError(Throwable e) {
                        pd.ocultar();
                        Toast.makeText(getApplicationContext(), getString(R.string.error_lectura_modo), Toast.LENGTH_SHORT).show();
                        Log.e(TAG, "Error de conexión con el servidor en la lectura de la IP Pública de Raspberry Pi");
                    }

                    @Override
                    public void onNext(Response<List<IPPublica>> ipPublicaResponse) {

                        // Variable para la obtención del resultado de cada petición
                        int codigoPeticion = ipPublicaResponse.code();
                        // Reseteamos las variables auxiliares de error de credenciales

                        if (codigoPeticion == 200) {

                            try {
                                // Guardamos el valor de la respuesta en una variable auxiliar
                                ippublica = ipPublicaResponse.body().get(0).getIp();
                                url = "https://" + usuario + ":" + password + "@" + ippublica + ":" + puerto +"/stream?width=328&hight=328";
                                Log.e(TAG, "La url completa es: " + url);
                                // Cargamos la URL en el navegador
                                navegador.loadUrl(url);

                            } catch (Exception oe) {
                                oe.printStackTrace();
                            }

                        // La petición ha sido ejecutada correctamente pero el token JWT usado ha expirado o no se ha referenciado en la cabecera
                        } else if (codigoPeticion == 401) {

                            Toast.makeText(getApplicationContext(), getString(R.string.error_lectura_modo), Toast.LENGTH_SHORT).show();

                        }

                    }
                });
    }

}
