package es.upm.sidocs.sidocs.utils;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import static android.content.Context.MODE_PRIVATE;


public class UserSharedPreferences {


    public static String getTokenJWT(Context context){

        SharedPreferences preferences = context.getSharedPreferences("sidocspref", MODE_PRIVATE);
        String tokenJWT = preferences.getString("tokenJWT", null);
        return tokenJWT;

    }

    public static void setTokenJWT(Context context, String tokenJWT){

        SharedPreferences preferences = context.getSharedPreferences("sidocspref", MODE_PRIVATE);
        if(tokenJWT == null){

            preferences.edit().putString("tokenJWT", tokenJWT).apply();
        }
        else {
            preferences.edit().putString("tokenJWT", "JWT " + tokenJWT).apply();
        }
    }

    public static String getTokenFCM(Context context){

        SharedPreferences preferences = context.getSharedPreferences("sidocspref", MODE_PRIVATE);
        String tokenFCM = preferences.getString("tokenFCM", null);
        return tokenFCM;
    }

    public static void setTokenFCM(Context context, String tokenFCM){

        SharedPreferences preferences = context.getSharedPreferences("sidocspref", MODE_PRIVATE);
        preferences.edit().putString("tokenFCM", tokenFCM).apply();
    }

    public static Boolean getNotificaciones(Context context){

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        Boolean notificaciones = preferences.getBoolean("notificaciones", false);
        return notificaciones;
    }

    public static String getUsuario(Context context){

        SharedPreferences preferences = context.getSharedPreferences("sidocspref", MODE_PRIVATE);
        String usuario = preferences.getString("usuario", null);
        return usuario;
    }

    public static void setUsuario(Context context, String usuario){

        SharedPreferences preferences = context.getSharedPreferences("sidocspref", MODE_PRIVATE);
        preferences.edit().putString("usuario", usuario).apply();
    }

    public static String getPassword(Context context){

        SharedPreferences preferences = context.getSharedPreferences("sidocspref", MODE_PRIVATE);
        String password = preferences.getString("password", null);
        return password;
    }

    public static void setPassword(Context context, String password){

        SharedPreferences preferences = context.getSharedPreferences("sidocspref", MODE_PRIVATE);
        preferences.edit().putString("password", password).apply();
    }

    public static void setNotificaciones(Context context, Boolean notificaciones){

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        preferences.edit().putBoolean("notificaciones", notificaciones).apply();
    }

    public static String getModo(Context context){

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        String modo = preferences.getString("modo", "1");
        return modo;
    }

    public static void setModo(Context context, String modo){

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        preferences.edit().putString("modo", modo).apply();
    }

    public static String getModoActual(Context context){

        SharedPreferences preferences = context.getSharedPreferences("sidocspref", MODE_PRIVATE);
        String modo = preferences.getString("modo", "1");
        return modo;
    }

    public static void setModoActual(Context context, String modo){

        SharedPreferences preferences = context.getSharedPreferences("sidocspref", MODE_PRIVATE);
        preferences.edit().putString("modo", modo).apply();
    }

}
