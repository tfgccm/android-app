package es.upm.sidocs.sidocs.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UsuarioAccesoRespuesta {

    @SerializedName("token")
    @Expose
    private String token;

    @SerializedName("non_field_errors")
    @Expose
    private String error_credenciales;

    public String getToken() {
        return token;
    }
    public void setToken(String token) {
        this.token = token;
    }

    public String getError_credenciales() {
        return error_credenciales;
    }
    public void setError_credenciales(String error_credenciales) {
        this.error_credenciales = error_credenciales;
    }

    @Override
    public String toString() {
        return "Usr_Acs_Res{" +
                "token='" + token + '\'' +
                '}';
    }
}
