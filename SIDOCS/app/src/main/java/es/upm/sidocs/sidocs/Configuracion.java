package es.upm.sidocs.sidocs;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.List;

import es.upm.sidocs.sidocs.data.model.ModoFuncionamientoActualizar;
import es.upm.sidocs.sidocs.data.model.ModoFuncionamientoLeer;
import es.upm.sidocs.sidocs.data.model.UsuarioRegistroFCM;
import es.upm.sidocs.sidocs.data.remote.APIInterface;
import es.upm.sidocs.sidocs.data.remote.APIUtils;
import es.upm.sidocs.sidocs.utils.UserProgressDialog;
import es.upm.sidocs.sidocs.utils.UserSharedPreferences;
import retrofit2.Response;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class Configuracion extends PreferenceActivity {

    // Tag identificativo para Logcat
    public final String TAG = "SIDOCS_Configuracion";

    // Ventana de carga para el proceso de guardado de cambios de configuración
    private UserProgressDialog pd;

    // Instancia de APIInterface para establecer las peticiones al servidor REST
    private APIInterface servicioApi;

    // Variables auxiliares necesarias para detectar si el usuario ha cambiado el modo de funcionamiento
    public Integer modoId;
    public String modoId_str, modoId_aux_str;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction().replace(android.R.id.content, new SidocsPreferenceFragment()).commit();

        // Creamos una ventana de carga de guardado de cambios de configuración
        pd = new UserProgressDialog(this,
                getString(R.string.titulo_guardar_config),
                getString(R.string.cuerpo_guardar_config));

        // Asignamos a la instancia APIInterface el
        servicioApi = APIUtils.getAPIService(getApplicationContext(),0);


    }

    public static class SidocsPreferenceFragment extends PreferenceFragment {
        @Override
        public void onCreate(final Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.configuracion);
        }
    }

    public void ejecutar_menuPrincipal() {

        Intent i = new Intent(this, MenuPrincipal.class);
        startActivity(i);

    }

    @Override
    public void onBackPressed() {

        if (comprobarCambios()) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.VentanaDialogo)
                    .setCancelable(true)
                    .setTitle(getString(R.string.titulo_guardar_config))
                    .setMessage(getString(R.string.cuerpo_guardar_config));

            builder.setNegativeButton(getString(R.string.cancelar), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface di, int i) {
                    di.cancel();
                }
            });

            builder.setPositiveButton(getString(R.string.aceptar), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface di, int i) {

                    di.dismiss();
                    String tokenJWT = UserSharedPreferences.getTokenJWT(Configuracion.this);
                    pd.mostrar();
                    actualizarModoFuncionamientoAPI(tokenJWT);

                }
            });
            builder.create().show();
        }
        else{
            super.onBackPressed();
        }
    }

    public boolean comprobarCambios(){

        // Obtenemos el modo de funcionamiento actual del sistema y el escogido por el usuario
        String modoActual = UserSharedPreferences.getModoActual(Configuracion.this);
        String modoNuevo = UserSharedPreferences.getModo(Configuracion.this);

        // Evaluamos ambos valores
        if(!modoActual.equals(modoNuevo)){
            return true;
        }
        else{
            return false;
        }

    }

    public void actualizarModoFuncionamientoAPI(String tokenJWT) {

        // Creamos una instacia de ModoFuncionamientoActualizar con los datos introducidos por el usuarios
        modoId_str = UserSharedPreferences.getModo(Configuracion.this);
        modoId = Integer.parseInt(modoId_str);
        ModoFuncionamientoActualizar body = new ModoFuncionamientoActualizar(modoId);

        servicioApi.leerModoFuncionamiento(tokenJWT)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(throwable ->  {

                    Log.e(TAG, "Error de conexión con el servidor en la lectura de la id del último modo de funcionamiento del sistema");

                })
                // Usamos la operación concatMap para pasar por parámetro de cabecera el id del modo de funcionamiento
                .concatMap(
                        // Usamos el id obtenido de la primera petición como parámetro de la URL para actualizar el modo de funcionamiento
                        modoResponse -> servicioApi.actualizarModoFuncionamiento(modoResponse.body().get(0).getId(), body ,tokenJWT )
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                )
                .subscribe(new Subscriber<Response<ModoFuncionamientoLeer>>() {

                    @Override
                    public void onCompleted() {

                        pd.ocultar();
                        ejecutar_menuPrincipal();

                    }

                    @Override
                    public void onError(Throwable e) {

                        Log.e(TAG, "Error de conexión con el servidor en la actualización del modo de funcionamiento");
                        Toast.makeText(getApplicationContext(), "Error de conexión. No se han podido guardar los cambios", Toast.LENGTH_SHORT).show();
                        pd.ocultar();
                        ejecutar_menuPrincipal();

                    }

                    @Override
                    public void onNext(Response<ModoFuncionamientoLeer> response) {


                        // Variable para la obtención del resultado de cada petición
                        int codigoPeticion = response.code();

                        if (codigoPeticion == 200) {

                            UserSharedPreferences.setModoActual(Configuracion.this, modoId_str);
                            Toast.makeText(getApplicationContext(), getString(R.string.configuracion_correcto), Toast.LENGTH_SHORT).show();

                        // La petición ha sido ejecutada correctamente pero el token JWT usado ha expirado o no se ha referenciado en la cabecera
                        } else if (codigoPeticion == 401) {

                            Toast.makeText(getApplicationContext(), getString(R.string.error_actualizacion_modo), Toast.LENGTH_SHORT).show();


                        }
                    }
                });
    }
}
