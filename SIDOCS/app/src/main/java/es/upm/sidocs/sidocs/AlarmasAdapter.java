package es.upm.sidocs.sidocs;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;

import es.upm.sidocs.sidocs.data.model.ListadoAlarmas;

public class AlarmasAdapter extends RecyclerView.Adapter<AlarmasAdapter.AlarmasViewHolder> {

    Context context;
    List<ListadoAlarmas> alarmasListado;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder

    // Create new views (invoked by the layout manager)
    @Override
    public AlarmasAdapter.AlarmasViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View v = layoutInflater.inflate(R.layout.alarma_elemento, parent, false);

        AlarmasViewHolder vh = new AlarmasViewHolder(v);
        return vh;
    }


    public class AlarmasViewHolder extends RecyclerView.ViewHolder {

        TextView titulo;
        TextView cuerpo;
        TextView fecha;
        ImageView image;

        public AlarmasViewHolder(View v) {
            super(v);

            titulo = (TextView) v.findViewById(R.id.id_alarma_titulo);
            cuerpo = (TextView) v.findViewById(R.id.id_alarma_cuerpo);
            fecha = (TextView)  v.findViewById(R.id.id_alarma_fecha);
            image = (ImageView) v.findViewById(R.id.id_alarma_icono);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public AlarmasAdapter(Context context, List<ListadoAlarmas> alarmasListado) {

        this.context = context;
        this.alarmasListado = alarmasListado;

    }

    public void setAlarmasListado(List<ListadoAlarmas> alarmasListado) {
        this.alarmasListado = alarmasListado;
        notifyDataSetChanged();
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(AlarmasAdapter.AlarmasViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        holder.titulo.setText("Alarma de " + alarmasListado.get(position).getTipoSensor().getNombre());
        holder.cuerpo.setText(alarmasListado.get(position).getMensaje().getDescripcion());
        holder.fecha.setText(alarmasListado.get(position).getFechaAlarma());

        int tipoSensor = alarmasListado.get(position).getTipoSensor().getId();
        int drawableImagen;
        switch(tipoSensor){

            case 3: // Alarma por inundación
                drawableImagen = R.drawable.ic_alarma_agua;
                break;
            case 4: // Alarma por presencia de gas
                drawableImagen = R.drawable.ic_alarma_gas;
                break;
            case 5: // Alarma por detección de movimiento
                drawableImagen = R.drawable.ic_alarma_movimiento;
                break;
            default:
                drawableImagen = R.drawable.sidocs_icon2;
        }

        holder.image.setImageResource(drawableImagen);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {

        if(alarmasListado != null){

            return alarmasListado.size();

        }
        else
            return 0;
    }

}
