package es.upm.sidocs.sidocs.service;
import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.content.Intent;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Random;

import es.upm.sidocs.sidocs.Acceso;
import es.upm.sidocs.sidocs.MenuPrincipal;
import es.upm.sidocs.sidocs.R;
import es.upm.sidocs.sidocs.data.model.ListadoAlarmas;
import es.upm.sidocs.sidocs.utils.UserSharedPreferences;


public class SidocsFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "SidocsFirebaseMsgService";

    // onMessageReceived se llama cuando un mensaje es recibido por la aplicación, activa o en segundo plano.
    @Override
    public void onMessageReceived(RemoteMessage message) {
        super.onMessageReceived(message);

        String title = null;
        String body = null;

        // Se accede al contenido del mensaje de datos recibido y se asigna al título y cuerpo de la notificación
        if(message.getData() != null){
            title = message.getData().get("title");
            body = message.getData().get("body");

        }

        // Se obtiene el estado de la configuración de Notificaciones por parte del usuario y
        // se muestra el mensaje sólo si están activadas.

        Boolean notificaciones= UserSharedPreferences.getNotificaciones(this);

        if(notificaciones){

            showNotification(title, body);

        }
    }

    // onNewToken se llama cuando un nuevo token se ha generado.
    // Específicamente se invoca:
    //      1) Al instalar la aplicación, cuando el token se genera por primera vez.
    //      2) Si el token cambia, al "BORRAR DATOS" de la aplicación desde el menú de ajustes de Android.

    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token: " + token);
        super.onNewToken(token);
    }

    private void showNotification(String title, String body) {
        Context context;
        SharedPreferences sp;
        Intent intent;

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String channelId = getString(R.string.default_notification_channel_id);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        // Se obtiene el valor de tokenJWT de SharedPreferences para comprobar si el usaurio dispone de una sesión activa o no
        context = this.getBaseContext();
        sp = context.getSharedPreferences("sidocspref", MODE_PRIVATE);
        String tokenJWT = sp.getString("tokenJWT", null);

        // Si el tokenJWT es distinto a null, el usuario tiene una sesión activa en la aplicación, en ese caso se accede a la actividad de Listado de Alarmas
        if(tokenJWT != null) {

            intent = new Intent(this, ListadoAlarmas.class); // Tras hacer click sobre la notificación, se accede directamente al listado de notificaciones
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);            // Una vez el usuario ha pulsado sobre la notificación, ésta desaparece
        }
        // Si el tokenJWT es null, el usuario no tiene una sesión activa en la aplicación, en ese caso acceder a la actividad de Acceso
        else{

            intent = new Intent(this, Acceso.class); // Tras hacer click sobre la notificación, se accede directamente al listado de notificaciones
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);            // Una vez el usuario ha pulsado sobre la notificación, ésta desaparece
        }

        // Nota: Se añade un ID aleatorio para que las notificaciones se ecolen y no se sobreescriban
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);


        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {

            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;

            NotificationChannel notificationChannel = new NotificationChannel(channelId, name,importance);
            notificationChannel.setDescription(description);

            notificationManager.createNotificationChannel(notificationChannel);

        }

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.mipmap.ic_sidocs_notification)
                        .setContentTitle(title)
                        .setContentText(body)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        notificationManager.notify(null,new Random().nextInt(), notificationBuilder.build());

    }
}
