package es.upm.sidocs.sidocs;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import es.upm.sidocs.sidocs.data.model.UsuarioAcceso;
import es.upm.sidocs.sidocs.data.model.UsuarioRegistroFCM;
import es.upm.sidocs.sidocs.data.remote.APIInterface;
import es.upm.sidocs.sidocs.data.remote.APIUtils;
import es.upm.sidocs.sidocs.utils.UserProgressDialog;
import retrofit2.Response;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import es.upm.sidocs.sidocs.utils.UserSharedPreferences;

public class Acceso extends AppCompatActivity {

    // Tag identificativo para Logcat
    public final String TAG = "SIDOCS_Acceso";

    // Ventanta de carga para el proceso de acceso
    private UserProgressDialog  pd;

    // Instancia de APIInterface para establecer las peticiones al servidor REST
    private APIInterface servicioApi;

    // Vistas para obtener las credenciales del usuario y botón para ejecutar el acceso
    private EditText it_Usuario, it_Password;
    private TextInputLayout it_Layout_Usuario, it_Layout_Password;
    private TextView t_Registrar_Enlace;
    private Button b_acceder;

    // Declaración de variables UsuarioAcceso
    private String usuario;
    private String password;

    // Declaración de variables UsuarioRegistroFCM
    private String registration_id;
    public boolean active = true; // Por defecto, usuario-dispositivo activo
    public String type = "android"; // Por defecto, tipo de dispositivo "Android"

    // Variable auxiliar para almacenar el token FCM del dispositivo
    String tokenFCMActual;

    // Variables auxiliares para la obtención de la respuesta del servidor y eliminación de caracteres sobrantes de la misma
    String auxErrorCredenciales = null;
    String clean_auxErrorCredenciales = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // Se establece de nuevo el Tema Principal "AppTheme" después de haber cargado el splash
        setTheme(R.style.AppTheme);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acceso);

        // Se crea una ventana de carga
        pd = new UserProgressDialog(this,
                                            getString(R.string.titulo_acceso),
                                            getString(R.string.mensaje_acceso));

        servicioApi = APIUtils.getAPIService(getApplicationContext(),0);

        // Se asignan las instancias creadas con las vistas
        it_Usuario = (EditText) findViewById(R.id.id_it_login_usuario);
        it_Password = (EditText) findViewById(R.id.id_it_login_password);
        it_Layout_Usuario = (TextInputLayout) findViewById(R.id.id_it_login_usuario_layout);
        it_Layout_Password = (TextInputLayout) findViewById(R.id.id_it_login_password_layout);
        t_Registrar_Enlace = (TextView) findViewById(R.id.id_t_registrar);
        b_acceder = (Button) findViewById(R.id.id_b_acceder);

        /*
        // Se simplifican los métodos con expresiones Lambda

        // Esta expresión:

        vista_que_ejecuta_el_metodo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                metodo();
            }
        });

        // Se simplifica en:

        vista_que_ejecuta_el_metodo.setOnClickListener(view -> metodo());

        */
        obtenerTokenFCM();

        b_acceder.setOnClickListener(view -> acceder());

        t_Registrar_Enlace.setOnClickListener(view -> ejecutarRegister(view));


    }

    public void ejecutarRegister(View view) {

        Intent i = new Intent(this, Registro.class);
        startActivity(i);

    }

    public void ejecutarMenu() {
            Intent i = new Intent(this, MenuPrincipal.class);
            startActivity(i);
    }

    private void acceder() {

        boolean val_usr, val_pwd; // Variables auxiliares de validación

        // Se ejecuta la validación de todos los campos del formulario
        val_usr = validarUsuario();
        val_pwd = validarPassword();

        // Se comprueba si la validación ha sido satisfactoria
        if (val_usr && val_pwd) {

            // Éxito en la validación, por tanto se ejecuta la llamada al servidor REST
            Log.i(TAG, "Datos Validados");
            // Mostramos la ventana de carga
            pd.mostrar();
            // Éxito en la validación, por tanto se ejecuta la llamada al servidor REST
            accederAPI();

        }
    }

    private boolean validarUsuario() {
        if (it_Usuario.getText().toString().trim().isEmpty()) {
            it_Layout_Usuario.setErrorEnabled(true);
            it_Layout_Usuario.setError(getString(R.string.error_campo_vacio));
            return false;
        } else {
            it_Layout_Usuario.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validarPassword() {
        if (it_Password.getText().toString().trim().isEmpty()) {
            it_Layout_Password.setErrorEnabled(true);
            it_Layout_Password.setError(getString(R.string.error_campo_vacio));
            return false;
        } else {
            it_Layout_Password.setErrorEnabled(false);
        }

        return true;
    }

    public void obtenerTokenFCM(){

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(Acceso.this, new OnSuccessListener<InstanceIdResult>() {

            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {

                // Se obtiene el valor del token FCM actual
                tokenFCMActual = instanceIdResult.getToken();
                Log.i(TAG, "El token FCM es: " + tokenFCMActual);
                // Se guarda el valor del token FCM en SharedPreferences
                UserSharedPreferences.setTokenFCM(Acceso.this,tokenFCMActual);
            }
        });
    }

    public void accederAPI() {

        // Se crea una instacia de UsuarioRegistro con los datos introducidos por el usuarios y
        // se suprimen los posibles espacios en blanco a izquierda y derecha de la cadena de texto
        usuario = it_Usuario.getText().toString().trim();
        password = it_Password.getText().toString();
        UsuarioAcceso body = new UsuarioAcceso(usuario, password);


        // Se crea una instacia de UsuarioRegistroFCM con los datos introducidos por el usuarios
        registration_id = tokenFCMActual;
        UsuarioRegistroFCM body2 = new UsuarioRegistroFCM(registration_id, active, type);

                servicioApi.acceder(body)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnNext(usuarioAccesoRespuestaResponse-> {

                            // Variable para la obtención del resultado de cada petición
                            int codigoPeticion = usuarioAccesoRespuestaResponse.code();
                            auxErrorCredenciales = null;
                            clean_auxErrorCredenciales = null;

                            // Se resetean las variables auxiliares de error de credenciales
                            if (codigoPeticion == 200) {

                                try {

                                    // Se obtiene el token JWT generado por el servidor API REST, a partir de la respuesta de la petición
                                    String tokenJWT = usuarioAccesoRespuestaResponse.body().getToken();
                                    String usuario_aux = body.getUsername();
                                    String password_aux = body.getPassword();

                                    Log.i(TAG, "El token JWT obtenido es: " + usuarioAccesoRespuestaResponse.body().getToken());

                                    // Se guarda el token de acceso JWT en SharedPreferences en modo privado
                                    UserSharedPreferences.setTokenJWT(Acceso.this, tokenJWT);

                                    // Se guardan las credenciales del usuario para el proceso de reautenticación tras la expiración del token
                                    UserSharedPreferences.setUsuario(Acceso.this, usuario_aux);
                                    UserSharedPreferences.setPassword(Acceso.this, password_aux);


                                } catch (Exception oe) {
                                    oe.printStackTrace();
                                }


                            // La petición ha sido ejecutada correctamente, pero las credenciales enviadas por el usuario no son correctas
                            } else if (codigoPeticion == 400) {

                                try {

                                    // Se accede al contenido de la respuesta
                                    String response_error = usuarioAccesoRespuestaResponse.errorBody().string();
                                    try {

                                        // Se crea un objeto JSON para almacenar el resultado de la petición 400, ya que una vez se ha accedido
                                        // al contenido de response.errorBody().string() se referencia a null y el resultado de la petición se perdería

                                        JSONObject jsonError = new JSONObject(response_error);

                                        // Se comprueba si el objeto JSON recibido contiene errores de validación por usuario o password (non_field_errors)
                                        if (!jsonError.isNull("non_field_errors")) {

                                            // Objeto JSON auxiliar para almacenar el resultado de la petición con resultado 400
                                            auxErrorCredenciales = jsonError.getString("non_field_errors");
                                            // Se eliminan los caracteres [" y "] de la cadena recibida
                                            clean_auxErrorCredenciales = auxErrorCredenciales.replace("[\"", "").replace("\"]", "");

                                        }

                                    } catch (JSONException je) {
                                        je.printStackTrace();

                                    }
                                } catch (IOException ie) {
                                    ie.printStackTrace();
                                }

                            }

                        })
                        .doOnError(throwable ->  {
                            Log.e(TAG, "Error de conexión con el servidor en la autenticación de acceso");

                            // Se borran las variables auxiliares del mensaje de error en el acceso
                            auxErrorCredenciales = null;
                            clean_auxErrorCredenciales = null;

                            // Se borran de SharedPreferences el token JWT, el token FCM y las credenciales del usuario
                            UserSharedPreferences.setTokenJWT(Acceso.this, null);
                            UserSharedPreferences.setUsuario(Acceso.this, null);
                            UserSharedPreferences.setPassword(Acceso.this, null);
                        })
                        .unsubscribeOn(Schedulers.io())
                        // Se usa la operación concatMap para pasar por parámetro de cabecera el token de autenticación JWT
                        .concatMap(
                                accesoRespuestaResponse -> servicioApi.registrarFCM( body2 ,
                                        "JWT " + (accesoRespuestaResponse.body().getToken()))
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                )
                        .subscribe(new Subscriber<Response<UsuarioRegistroFCM>>() {

                    @Override
                    public void onCompleted() {

                        pd.ocultar();

                    }

                    @Override
                    public void onError(Throwable e) {

                        pd.ocultar();
                        // Se borran de SharedPreferences el token JWT, el token FCM y las credenciales del usuario
                        UserSharedPreferences.setTokenJWT(Acceso.this, null);
                        UserSharedPreferences.setUsuario(Acceso.this, null);
                        UserSharedPreferences.setPassword(Acceso.this, null);

                        Log.e(TAG, "Error de conexión con el servidor en el registro FCM");

                        if(clean_auxErrorCredenciales!=null) {

                            Toast.makeText(getApplicationContext(), clean_auxErrorCredenciales, Toast.LENGTH_LONG).show();
                            // En caso contrario el motivo por el que se ha ejecutado el método onError es porque no se ha ejecutado correctamente la petición
                            // en la primera llamada o en la segunda

                        }else {

                            Toast.makeText(getApplicationContext(), getString(R.string.error_conexion_servidor), Toast.LENGTH_LONG).show();

                        }

                    }

                    @Override
                    public void onNext(Response<UsuarioRegistroFCM> response) {

                        pd.ocultar();

                        int codigoPeticion = response.code();

                        // La petición ha sido ejecutada correctamente y el dispositivo se ha registrado al usuario en la API REST o
                        // el dispositivo ya estaba previamente registrado para el usuario solicitante
                        if (codigoPeticion == 201 || codigoPeticion == 400) {

                            try {

                                // Se limpia el formulario de acceso
                                it_Usuario.getText().clear();
                                it_Password.getText().clear();

                                // Se activan por defecto las notificaciones
                                UserSharedPreferences.setNotificaciones(Acceso.this, true);


                                // Mensaje emergente: Éxito en el acceso
                                Toast.makeText(getApplicationContext(), getString(R.string.acceso_correcto), Toast.LENGTH_SHORT).show();

                                // Se ejecuta la actividad del menú principal
                                ejecutarMenu();


                            } catch (Exception oe) {
                                oe.printStackTrace();
                            }

                        // La petición ha sido ejecutada correctamente pero el token JWT usado ha expirado o no se ha referenciado en la cabecera
                        } else if (codigoPeticion == 401) {

                            Toast.makeText(getApplicationContext(), getString(R.string.error_registro_dispositivo), Toast.LENGTH_SHORT).show();

                        }
                    }
                });
    }

    // Se sobreescribe el método onBackPressed() de retorno de actividad para evitar regresar a la actividad de MenuPrincipal y
    // por el contrario salir de la aplicación.
    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.VentanaDialogo)
                .setCancelable(true)
                .setTitle(getString(R.string.titulo_mensaje_salir))
                .setMessage(getString(R.string.cuerpo_mensaje_salir));

        builder.setNegativeButton(getString(R.string.cancelar), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });

        builder.setPositiveButton(getString(R.string.aceptar), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface di, int i) {

                Intent salirItent = new Intent(Intent.ACTION_MAIN);
                salirItent.addCategory( Intent.CATEGORY_HOME );
                salirItent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(salirItent);
            }
        });
        builder.create().show();
    }
}



