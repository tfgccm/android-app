package es.upm.sidocs.sidocs;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import es.upm.sidocs.sidocs.data.model.Humedad;
import es.upm.sidocs.sidocs.data.model.Temperatura;
import es.upm.sidocs.sidocs.data.model.Unidad;
import es.upm.sidocs.sidocs.data.remote.APIInterface;
import es.upm.sidocs.sidocs.data.remote.APIUtils;
import es.upm.sidocs.sidocs.utils.UserProgressDialog;
import es.upm.sidocs.sidocs.utils.UserSharedPreferences;
import retrofit2.Response;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class Sensores extends AppCompatActivity {

    // Tag identificativo para Logcat
    public final String TAG = "SIDOCS_Sensores";

    // Ventanta de carga para el proceso de lectura de sensores
    private UserProgressDialog pd;

    // Instancia de APIInterface para establecer las peticiones al servidor REST
    private APIInterface servicioApi;

    // Vistas para representar la Temperatura, Humedada y Unidades
    private TextView t_Temperatura, t_Humedad, t_Temperatura_unidad, t_Humedad_unidad;

    // Variables auxiliares para guardar los resultados de las peticiones
    public String t_Temperatura_aux, t_Humedad_aux, t_Temperatura_unidad_aux,  t_Humedad_unidad_aux;
    public boolean acabado1, acabado2 = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensores);

        // Se crea una ventana de carga
        pd = new UserProgressDialog(this,
                                            getString(R.string.titulo_sensores),
                                            getString(R.string.cuerpo_sensores));

        servicioApi = APIUtils.getAPIService(getApplicationContext(), 0);

        // Se asignan las instancias creadas con las vistas
        t_Temperatura = (TextView) findViewById(R.id.id_t_Temperatura);
        t_Humedad = (TextView) findViewById(R.id.id_t_Humedad);
        t_Temperatura_unidad = (TextView) findViewById(R.id.id_t_Temperatura_unidades);
        t_Humedad_unidad = (TextView) findViewById(R.id.id_t_Humedad_unidades);

        // Se obtiene el valor del token JWT de Shared Preferences
        String tokenJWT = UserSharedPreferences.getTokenJWT(Sensores.this);
        // Se ejecuta la llamada a la API REST para obtener los valores al inicio de la Actividad
        leerSensoresAPI(tokenJWT);
    }

    public void leerSensoresAPI(String tokenJWT){

        // Se muestra la ventana de carga
        pd.mostrar();
        // Se obtienen valor y unidad de Temperatura a través de la llamada a la API REST
        leerTemperaturaAPI(tokenJWT);
        // Se obtienen valor y unidad de Humedad a través de la llamada a la API REST
        leerHumedadAPI(tokenJWT);

    }

        public void leerTemperaturaAPI(String tokenJWT) {

        servicioApi.leerTemperatura(tokenJWT)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<List<Temperatura>>>() {

                    @Override
                    public void onCompleted() {

                        // Petición finalizada
                        acabado1 = true;
                        // Si ha finalizado ya la otra petición, se oculta la ventana de carga
                        if (acabado2){
                            pd.ocultar();
                        }

                    }

                    @Override
                    public void onError(Throwable e) {

                        // Petición finalizada
                        acabado1 = true;
                        // Si ha finalizado ya la otra petición, se oculta la ventana de carga
                        if (acabado2){
                            pd.ocultar();
                        }

                        Log.e(TAG, "Error de conexión con el servidor en la lectura de las unidades de Temperatura");

                    }

                    @Override
                    public void onNext(Response<List<Temperatura>> response) {

                        // Variable para la obtención del resultado de cada petición
                        int codigoPeticion = response.code();

                        if (codigoPeticion == 200) {

                        try {
                            // Puesto que se trata de una lista de un solo elemento,
                            // accedemos a su valor con el método get(posición_en_la_lista) [en este caso 0] y
                            // lo guardamos en una variable auxiliar
                            t_Temperatura_aux = response.body().get(0).getValor();
                            // Guardamos el valor en una variable auxiliar
                            t_Temperatura_unidad_aux = response.body().get(0).getUnidad().getSimbolo();
                            // Representamos el valor obtenido en el Text View
                            t_Temperatura.setText(t_Temperatura_aux);
                            // Representamos las unidades obtenidas en el Text View
                            t_Temperatura_unidad.setText(t_Temperatura_unidad_aux);


                        } catch (Exception oe) {
                            oe.printStackTrace();
                        }



                        // La petición ha sido ejecutada correctamente pero el token JWT usado ha expirado o no se ha referenciado en la cabecera
                    } else if (codigoPeticion == 401) {

                        Toast.makeText(getApplicationContext(), getString(R.string.error_lectura_temperatura), Toast.LENGTH_SHORT).show();

                        }
                    }
                });

    }

    public void leerHumedadAPI(String tokenJWT) {

        servicioApi.leerHumedad(tokenJWT)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<List<Humedad>>>() {

                    @Override
                    public void onCompleted() {

                        // Petición finalizada
                        acabado2 = true;
                        // Si ha finalizado ya la otra petición, se oculta la ventana de carga
                        if (acabado1){
                            pd.ocultar();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                        // Petición finalizada
                        acabado2 = true;
                        // Si ha finalizado ya la otra petición, se oculta la ventana de carga
                        if (acabado1){
                            pd.ocultar();
                        }

                        Log.e(TAG, "Error de conexión con el servidor en la lectura de la Humedad");

                    }

                    @Override
                    public void onNext(Response<List<Humedad>> response) {

                        int codigoPeticion = response.code();

                        // La petición ha sido ejecutada correctamente

                        if (codigoPeticion == 200) {

                            try {

                                t_Humedad_aux = response.body().get(0).getValor();
                                // Se guarda el valor en una variable auxiliar
                                t_Humedad_unidad_aux = response.body().get(0).getUnidad().getSimbolo();
                                // Se representa el valor obtenido en el Text View
                                t_Humedad.setText(t_Humedad_aux);
                                // Se representa la unidad obtenida en el Text View
                                t_Humedad_unidad.setText(t_Humedad_unidad_aux);


                            } catch (Exception oe) {
                                oe.printStackTrace();
                            }

                        // La petición ha sido ejecutada correctamente pero el token JWT usado ha expirado o no se ha referenciado en la cabecera
                        } else if (codigoPeticion == 401) {

                            Toast.makeText(getApplicationContext(), getString(R.string.error_lectura_humedad), Toast.LENGTH_SHORT).show();

                        }
                    }
                });
    }
}
