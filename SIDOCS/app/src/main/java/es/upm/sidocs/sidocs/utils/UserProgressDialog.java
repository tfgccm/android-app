package es.upm.sidocs.sidocs.utils;
import android.app.ProgressDialog;
import android.content.Context;
import android.view.WindowManager.LayoutParams;

import es.upm.sidocs.sidocs.R;

public class UserProgressDialog extends ProgressDialog {

    public UserProgressDialog(Context context, String titulo, String mensaje){
        super(context,R.style.VentanaDialogo);
        super.setTitle(titulo);
        super.setMessage(mensaje);
        super.setCancelable(false);

    }
    public void mostrar(){
        super.show();
        super.getWindow().setLayout(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT);
    }

    public void ocultar(){
        if (super.isShowing()) {
            super.dismiss();
        }
    }
}
