package es.upm.sidocs.sidocs.data.model;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModoFuncionamientoLeer {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("modo")
    @Expose
    private ModoFuncionamientoTipoLeer modo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ModoFuncionamientoTipoLeer getModo() {
        return modo;
    }

    public void setModo(ModoFuncionamientoTipoLeer modo) {
        this.modo = modo;
    }
}
