package es.upm.sidocs.sidocs.data.model;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Temperatura {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("valor")
    @Expose
    private String valor;
    @SerializedName("unidad")
    @Expose
    private Unidad unidad;
    @SerializedName("fecha_lectura")
    @Expose
    private String fechaLectura;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public Unidad getUnidad() {
        return unidad;
    }

    public void setUnidad(Unidad unidad) {
        this.unidad = unidad;
    }

    public String getFechaLectura() {
        return fechaLectura;
    }

    public void setFechaLectura(String fechaLectura) {
        this.fechaLectura = fechaLectura;
    }
}
