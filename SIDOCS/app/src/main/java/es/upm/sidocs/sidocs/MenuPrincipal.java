package es.upm.sidocs.sidocs;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.List;

import es.upm.sidocs.sidocs.data.model.ModoFuncionamientoLeer;
import es.upm.sidocs.sidocs.data.model.UsuarioRegistroFCM;
import es.upm.sidocs.sidocs.data.remote.APIInterface;
import es.upm.sidocs.sidocs.data.remote.APIUtils;
import es.upm.sidocs.sidocs.utils.UserProgressDialog;
import es.upm.sidocs.sidocs.utils.UserSharedPreferences;
import retrofit2.Response;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MenuPrincipal extends AppCompatActivity {

    // Tag identificativo para Logcat
    public static final String TAG = "SIDOCS_MenuPrincipal";

    // Ventana de carga para el proceso de cierre de sesión y carga de parámetros de configuración
    private UserProgressDialog pd_cs, pd_cc;

    // Instancia de APIInterface para establecer las peticiones al servidor REST
    private APIInterface servicioApi;

    // Variables auxiliares necesarias para detectar si el usuario ha cambiado el modo de funcionamiento
    public Integer modoId;
    public String modoId_str;

    // Botones "CardView" del menú
    private View b_cv_sensores, b_cv_camara, b_cv_alarmas, b_cv_configuracion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_principal);

        // Se crea una ventana de carga de Cierre de Sesión
        pd_cs = new UserProgressDialog(this,
                getString(R.string.titulo_mensaje_cerrar_sesion),
                getString(R.string.cuerpo_mensaje_cerrar_sesion));

        // Se crea una ventana de carga de Configuración
        pd_cc = new UserProgressDialog(this,
                getString(R.string.titulo_cargar_config),
                getString(R.string.cuerpo_cargar_config));


        servicioApi = APIUtils.getAPIService(getApplicationContext(),0);

        // Se asignan las instancias creadas con las vistas
        b_cv_sensores = (View) findViewById(R.id.id_cv_sensores);
        b_cv_camara = (View) findViewById(R.id.id_cv_camara);
        b_cv_alarmas = (View) findViewById(R.id.id_cv_alarmas);
        b_cv_configuracion = (View) findViewById(R.id.id_cv_configuracion);

        // Se definen los métodos a ejecutar tras hacer click
        b_cv_sensores.setOnClickListener(view -> ejecutarSensores(view));
        b_cv_camara.setOnClickListener(view -> ejecutarCamara(view));
        b_cv_alarmas.setOnClickListener(view -> ejecutarAlarmas(view));
        b_cv_configuracion.setOnClickListener(view -> cargarConfiguracion(view));
    }


    public void ejecutarSensores(View view){

        Intent i = new Intent(this, Sensores.class);
        startActivity(i);

    }

    public void ejecutarCamara(View view){

        Intent i = new Intent(this, Camara.class);
        startActivity(i);

    }

    public void ejecutarAlarmas(View view){

        Intent i = new Intent(this, Alarmas.class);
        startActivity(i);

    }

    public void ejecutarConfiguracion(View view){

        Intent i = new Intent(this, Configuracion.class);
        startActivity(i);

    }

    public void cargarConfiguracion(View view){
        // Se carga la configuración guardada en el servidor antes de acceder a la actividad
        // en este caso se obtiene el modo de funcionamiento actual
        String tokenJWT = UserSharedPreferences.getTokenJWT(MenuPrincipal.this);
        pd_cc.mostrar();
        leerModoFuncionamientoAPI(tokenJWT, view);

    }

    public void ejecutarCreditos(){

        Intent i = new Intent(this, Creditos.class);
        startActivity(i);
    }


    // Se sobreescribe el método onBackPressed() de retorno de actividad para evitar regresar a la actividad de Acceso con una sesión activa.
    // De esta forma se solicita al usuario si desea cerrar la sesión para volver a "Acceso".
    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.VentanaDialogo)
                .setCancelable(true)
                .setTitle(getString(R.string.titulo_mensaje_salir))
                .setMessage(getString(R.string.cuerpo_mensaje_salir));

        builder.setNegativeButton(getString(R.string.cancelar), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface di, int i) {
                di.cancel();
            }
        });

        builder.setPositiveButton(getString(R.string.aceptar), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface di, int i) {

                di.dismiss();
                cerrarSesion();

            }
        });
        builder.create().show();
    }

    public void cerrarSesion() {

        // Se muestra la ventana de carga
        pd_cs.mostrar();

        // Se obtienen los token FCM y JWT para efectuar la llamada del método eliminarFCMAPI:
        // -tokenFCM como parámetro de URL para eliminar todos los registros de la tabla de dispositivos con registration_id = tokenFCM
        // -tokenJWT para autenticar la llamada
        String tokenFCM = UserSharedPreferences.getTokenFCM(MenuPrincipal.this);
        String tokenJWT = UserSharedPreferences.getTokenJWT(MenuPrincipal.this);
        eliminarFCMAPI(tokenFCM,tokenJWT);

    }

    public void eliminarFCMAPI(String tokenFCM, String tokenJWT) {

        servicioApi.eliminarFCM(tokenFCM, tokenJWT)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<UsuarioRegistroFCM>>() {

                    @Override
                    public void onCompleted() {
                        pd_cs.ocultar();
                    }

                    @Override
                    public void onError(Throwable e) {

                        Log.e(TAG, "Error en la llamada a la API para eliminar el dispositivo FCM.");
                        Toast.makeText(getApplicationContext(), "No se ha podido completar el cierre de sesión", Toast.LENGTH_LONG).show();
                        pd_cs.ocultar();
                    }

                    @Override
                    public void onNext(Response<UsuarioRegistroFCM> response) {


                        int statusCode = response.code(); // Variable para la obtención del resultado de cada petición

                        // Si la petición de borrado ha resultado satisfactoria
                        if (statusCode == 204){
                            // Se cierra la ventana de carga
                            pd_cs.ocultar();
                            // Se muestra un mensaje informativo
                            Toast.makeText(getApplicationContext(), "Cierre de sesión completado", Toast.LENGTH_SHORT).show();
                            // Se borra de SharedPreferences tanto el token JWT como el token FCM
                            UserSharedPreferences.setTokenFCM(MenuPrincipal.this, null);
                            UserSharedPreferences.setTokenJWT(MenuPrincipal.this, null);
                            // Se borran las credenciales almacenadas del usuario
                            UserSharedPreferences.setUsuario(MenuPrincipal.this, null);
                            UserSharedPreferences.setPassword(MenuPrincipal.this, null);

                            // Finaliza la actividad, volviendo a la actividad de acceso
                            Intent i = new Intent(getApplicationContext(), Acceso.class);
                            startActivity(i);

                        }
                        // La petición ha sido ejecutada correctamente pero ha habido un error en el servidor
                        else{
                            pd_cs.ocultar();
                            Toast.makeText(getApplicationContext(), "Fallo en el cierre de sesión FCM", Toast.LENGTH_SHORT).show();

                        }
                    }
                });

    }

    public void leerModoFuncionamientoAPI(String tokenJWT, View view){

        servicioApi.leerModoFuncionamiento(tokenJWT)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<List<ModoFuncionamientoLeer>>>() {

                    @Override
                    public void onCompleted() {
                        pd_cc.ocultar();
                        ejecutarConfiguracion(view);
                    }

                    @Override
                    public void onError(Throwable e) {
                        pd_cc.ocultar();

                        Toast.makeText(getApplicationContext(), getString(R.string.error_lectura_modo), Toast.LENGTH_SHORT).show();
                        Log.e(TAG, "Error de conexión con el servidor en la lectura del modo de funcionamiento");
                        ejecutarConfiguracion(view);
                    }

                    @Override
                    public void onNext(Response<List<ModoFuncionamientoLeer>> modoResponse) {

                        // Variable para la obtención del resultado de cada petición
                        int codigoPeticion = modoResponse.code();
                        // Se resetean las variables auxiliares de error de credenciales

                        if (codigoPeticion == 200) {

                            try {
                                // Se guarda el valor en una variable auxiliar
                                modoId = modoResponse.body().get(0).getModo().getId();
                                Log.i(TAG, "El modo de la API es, " + modoId);
                                modoId_str = String.valueOf(modoId);

                                UserSharedPreferences.setModoActual(MenuPrincipal.this, modoId_str);
                                UserSharedPreferences.setModo(MenuPrincipal.this, modoId_str);
                                Log.i(TAG, "El modo de DefaultSharedPreferences, " + UserSharedPreferences.getModo(MenuPrincipal.this));


                            } catch (Exception oe) {
                                oe.printStackTrace();
                            }

                            // La petición ha sido ejecutada correctamente, pero la autenticación de la petición ha fallado
                        } else if (codigoPeticion == 401) {

                            Toast.makeText(getApplicationContext(), getString(R.string.error_lectura_modo), Toast.LENGTH_SHORT).show();

                        }

                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_superior, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Se establece el método que ejecutará cada opción del menú superior
        switch (item.getItemId()) {
            case R.id.id_creditos:
                ejecutarCreditos();
                return true;
            case R.id.id_cerrar_sesion:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
