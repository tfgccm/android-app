package es.upm.sidocs.sidocs.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListadoAlarmas {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("tipo")
        @Expose
        private TipoSensor tipo;
        @SerializedName("mensaje")
        @Expose
        private Mensaje mensaje;
        @SerializedName("fecha_alarma")
        @Expose
        private String fechaAlarma;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public TipoSensor getTipoSensor() {
            return tipo;
        }

        public void setTipoSensor(TipoSensor tipo) {
            this.tipo = tipo;
        }

        public Mensaje getMensaje() {
            return mensaje;
        }

        public void setMensaje(Mensaje mensaje) {
            this.mensaje = mensaje;
        }

        public String getFechaAlarma() {
            return fechaAlarma;
        }

        public void setFechaAlarma(String fechaAlarma) {
            this.fechaAlarma = fechaAlarma;
        }

}
