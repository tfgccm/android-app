package es.upm.sidocs.sidocs.data.model;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UsuarioRegistroFCM {

    @SerializedName("registration_id")
    @Expose
    private String registration_id;
    @SerializedName("active")
    @Expose
    private boolean active;
    @SerializedName("type")
    @Expose
    private String type;

    public String getRegistration_id() {
        return registration_id;
    }

    public void setRegistration_id(String registration_id) {
        this.registration_id = registration_id;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public UsuarioRegistroFCM(String registration_id, boolean active, String type) {
        this.registration_id = registration_id;
        this.active = active;
        this.type = type;
    }

    @Override
    public String toString() {
        return "Usr_Reg_FCM{" +
                "registration_id='" + registration_id + '\'' +
                ", active='" + active + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
