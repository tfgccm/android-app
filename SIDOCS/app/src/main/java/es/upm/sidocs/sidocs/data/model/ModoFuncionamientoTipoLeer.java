package es.upm.sidocs.sidocs.data.model;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModoFuncionamientoTipoLeer {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("modo")
    @Expose
    private String modo;
    @SerializedName("descripcion")
    @Expose
    private String descripcion;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getModo() {
        return modo;
    }

    public void setModo(String modo) {
        this.modo = modo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}
