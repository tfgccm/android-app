package es.upm.sidocs.sidocs.data.model;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModoFuncionamientoActualizar {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("modo_id")
    @Expose
    private Integer modo_id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getModo_id() {
        return modo_id;
    }

    public void setModo_id(Integer modo_id) {
        this.modo_id = modo_id;
    }

    public ModoFuncionamientoActualizar(Integer modo_id) {
        this.modo_id = modo_id;
    }
}
